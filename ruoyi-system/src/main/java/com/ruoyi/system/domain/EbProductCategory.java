package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 商品分类对象 eb_product_category
 *
 * @author sopens
 * @date 2023-11-02
 */
public class EbProductCategory extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 分类id */
    private Long id;

    /** 父级ID */
    @Excel(name = "父级ID")
    private Long pid;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** icon */
    @Excel(name = "icon")
    private String icon;

    /** 级别:1，2，3 */
    @Excel(name = "级别:1，2，3")
    private Integer level;

    /** 排序 */
    @Excel(name = "排序")
    private Integer sort;

    /** 显示状态 */
    @Excel(name = "显示状态")
    private Boolean isShow;

    //查询创建时间
    @ApiModelProperty(value="查询创建时间")
    @JsonFormat(pattern ="yyyy-MM-dd HH:mm:ss")
    private String[] Times;

    //  开始时间
    @ApiModelProperty(value="开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String startTime;
    //  结束时间
    @ApiModelProperty(value="结束时间")
    @JsonFormat(pattern ="yyyy-MM-dd HH:mm:ss")
    private String endTime;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Boolean isDel;

    private List<EbProductCategory> children = new ArrayList<EbProductCategory>();

    public String[] getTimes() {
        return Times;
    }

    public void setTimes(String[] times) {
        Times = times;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setPid(Long pid)
    {
        this.pid = pid;
    }

    public Long getPid()
    {
        return pid;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
    public void setIcon(String icon)
    {
        this.icon = icon;
    }

    public String getIcon()
    {
        return icon;
    }
    public void setLevel(Integer level)
    {
        this.level = level;
    }

    public Integer getLevel()
    {
        return level;
    }
    public void setSort(Integer sort)
    {
        this.sort = sort;
    }

    public Integer getSort()
    {
        return sort;
    }
    public void setIsShow(Boolean isShow)
    {
        this.isShow = isShow;
    }

    public Boolean getIsShow()
    {
        return isShow;
    }
    public void setIsDel(Boolean isDel)
    {
        this.isDel = isDel;
    }

    public Boolean getIsDel()
    {
        return isDel;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("pid", getPid())
                .append("name", getName())
                .append("icon", getIcon())
                .append("level", getLevel())
                .append("sort", getSort())
                .append("isShow", getIsShow())
                .append("isDel", getIsDel())
                .append("createTime", getCreateTime())
                .append("updateTime", getUpdateTime())
                .append("startTime", getStartTime())
                .append("endTime", getEndTime())
                .append("Times", getTimes())
                .toString();
    }
}
