package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.EbProductCategoryMapper;
import com.ruoyi.system.domain.EbProductCategory;
import com.ruoyi.system.service.IEbProductCategoryService;

/**
 * 商品分类Service业务层处理
 *
 * @author sopens
 * @date 2023-11-02
 */
@Service
public class EbProductCategoryServiceImpl implements IEbProductCategoryService
{
    @Autowired
    private EbProductCategoryMapper ebProductCategoryMapper;

    /**
     * 查询商品分类
     *
     * @param id 商品分类主键
     * @return 商品分类
     */
    @Override
    public EbProductCategory selectEbProductCategoryById(Long id)
    {
        return ebProductCategoryMapper.selectEbProductCategoryById(id);
    }

    /**
     * 查询商品分类列表
     *
     * @param ebProductCategory 商品分类
     * @return 商品分类
     */
    @Override
    public List<EbProductCategory> selectEbProductCategoryList(EbProductCategory ebProductCategory)
    {
        if (ebProductCategory.getTimes() != null && ebProductCategory.getTimes().length>1 ){
            ebProductCategory.setStartTime(ebProductCategory.getTimes()[0]);
            ebProductCategory.setEndTime(ebProductCategory.getTimes()[1]);
        }
        return ebProductCategoryMapper.selectEbProductCategoryList(ebProductCategory);
    }

    /**
     * 新增商品分类
     *
     * @param ebProductCategory 商品分类
     * @return 结果
     */
    @Override
    public int insertEbProductCategory(EbProductCategory ebProductCategory)
    {
        ebProductCategory.setCreateTime(DateUtils.getNowDate());
        return ebProductCategoryMapper.insertEbProductCategory(ebProductCategory);
    }

    /**
     * 修改商品分类
     *
     * @param ebProductCategory 商品分类
     * @return 结果
     */
    @Override
    public int updateEbProductCategory(EbProductCategory ebProductCategory)
    {
        ebProductCategory.setUpdateTime(DateUtils.getNowDate());
        return ebProductCategoryMapper.updateEbProductCategory(ebProductCategory);
    }

    /**
     * 批量删除商品分类
     *
     * @param ids 需要删除的商品分类主键
     * @return 结果
     */
    @Override
    public int deleteEbProductCategoryByIds(Long[] ids)
    {
        return ebProductCategoryMapper.deleteEbProductCategoryByIds(ids);
    }

    /**
     * 删除商品分类信息
     *
     * @param id 商品分类主键
     * @return 结果
     */
    @Override
    public int deleteEbProductCategoryById(Long id)
    {
        return ebProductCategoryMapper.deleteEbProductCategoryById(id);
    }
}
