package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.EbProductCategory;

/**
 * 商品分类Service接口
 *
 * @author sopens
 * @date 2023-11-02
 */
public interface IEbProductCategoryService
{
    /**
     * 查询商品分类
     *
     * @param id 商品分类主键
     * @return 商品分类
     */
    public EbProductCategory selectEbProductCategoryById(Long id);

    /**
     * 查询商品分类列表
     *
     * @param ebProductCategory 商品分类
     * @return 商品分类集合
     */
    public List<EbProductCategory> selectEbProductCategoryList(EbProductCategory ebProductCategory);

    /**
     * 新增商品分类
     *
     * @param ebProductCategory 商品分类
     * @return 结果
     */
    public int insertEbProductCategory(EbProductCategory ebProductCategory);

    /**
     * 修改商品分类
     *
     * @param ebProductCategory 商品分类
     * @return 结果
     */
    public int updateEbProductCategory(EbProductCategory ebProductCategory);

    /**
     * 批量删除商品分类
     *
     * @param ids 需要删除的商品分类主键集合
     * @return 结果
     */
    public int deleteEbProductCategoryByIds(Long[] ids);

    /**
     * 删除商品分类信息
     *
     * @param id 商品分类主键
     * @return 结果
     */
    public int deleteEbProductCategoryById(Long id);
}
