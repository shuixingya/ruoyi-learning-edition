package com.ruoyi.system.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.util.ResourceUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.EbProductCategory;
import com.ruoyi.system.service.IEbProductCategoryService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * 商品分类Controller
 *
 * @author sopens
 * @date 2023-11-02
 */
@RestController
@RequestMapping("/system/category")
public class EbProductCategoryController extends BaseController {
    @Autowired
    private IEbProductCategoryService ebProductCategoryService;
    @Value("classpath:static/images")
    private Resource resource;
    private String separator = File.separator;//获取操作系统的文件分隔符
    private final ResourceLoader resourceLoader;
    public EbProductCategoryController(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    /**
     * 查询商品分类列表
     */
    @PreAuthorize("@ss.hasPermi('system:category:list')")
    @GetMapping("/list")
    public TableDataInfo list(HttpServletResponse response, EbProductCategory ebProductCategory) {
        System.out.println("======>" + ebProductCategory);
//        Map<String, Object> result = new HashMap<>();
//        // 将c, d, a的值直接放入结果Map中
//        result.put("id", ebProductCategory.getId());
//        result.put("pid", ebProductCategory.getPid());
//        result.put("name", ebProductCategory.getName());
//
//        // 获取b的值，如果b是一个Map
//        if (ebProductCategory.getTimes() instanceof Map) {
//            Map<String, Object> b = (Map<String, Object>) ebProductCategory.get("b");
//            result.put("b1", b.get("1"));
//            result.put("b2", b.get("2"));
//        }
        startPage();
        List<EbProductCategory> list = ebProductCategoryService.selectEbProductCategoryList(ebProductCategory);
        System.out.println("12312312321312===>" + list);
        return getDataTable(list);
    }

    /**
     * 导出商品分类列表
     */
    @PreAuthorize("@ss.hasPermi('system:category:export')")
    @Log(title = "商品分类", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, EbProductCategory ebProductCategory) {
        List<EbProductCategory> list = ebProductCategoryService.selectEbProductCategoryList(ebProductCategory);
        ExcelUtil<EbProductCategory> util = new ExcelUtil<EbProductCategory>(EbProductCategory.class);
        util.exportExcel(response, list, "商品分类数据");
    }

    /**
     * 获取商品分类详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:category:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(ebProductCategoryService.selectEbProductCategoryById(id));
    }

    /**
     * 新增商品分类
     */
    @PreAuthorize("@ss.hasPermi('system:category:add')")
    @Log(title = "商品分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody EbProductCategory ebProductCategory) {
        System.out.println(ebProductCategory);
        return toAjax(ebProductCategoryService.insertEbProductCategory(ebProductCategory));
    }

    /**
     * 修改商品分类
     */
    @PreAuthorize("@ss.hasPermi('system:category:edit')")
    @Log(title = "商品分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody EbProductCategory ebProductCategory) {
        return toAjax(ebProductCategoryService.updateEbProductCategory(ebProductCategory));
    }

    /**
     * 删除商品分类
     */
    @PreAuthorize("@ss.hasPermi('system:category:remove')")
    @Log(title = "商品分类", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(ebProductCategoryService.deleteEbProductCategoryByIds(ids));
    }

    /**
     * 文件上传
     * @param file 可以支持多文件上传
     * @return
     */
    @PostMapping(value = "testfileupload")
    public AjaxResult testfileupload(HttpServletRequest request, @RequestParam("file") MultipartFile file){
        String fileurl="";//用于页面回显的url
        try {
            if(file!= null){
                //获取文件名
                String fileName = file.getOriginalFilename();
                System.out.println("文件名:"+fileName);
                //获取文件后缀
//              String suffixName = fileName.substring(fileName.lastIndexOf("."));
//              System.out.println("文件后缀名:"+suffixName);
//              //对文件名sufixName进行判断
//              if(!suffixName.equals(".jpg")||!suffixName.equals(".png")||!suffixName.equals(".gif")){
//                  //返回错误信息
//                  return AjaxResult.error("图片格式不正确！");
//              }
                //判断file文件大小
                if(file.getSize() > 1024*1024*10){
                    return AjaxResult.error("图片大小不能超过10M！");
                }
                System.out.println("文件的类型getContentType:"+file.getContentType());
                //判断file文件类型
//                if(!file.getContentType().equals("image/jpeg") || !file.getContentType().equals("image/png") || !file.getContentType().equals("image/gif")){
//                    return AjaxResult.error("图片格式不正确！");
//                }
//            Resource myresorce= resourceLoader.getResource("classpath:static/images");//通过ResourceLoader获取资源目录对D:\Data\JavaHtml\RuoYi-Vue-master\calsspath:static\images/1691909660(1).jpg象
//            System.out.println("资源目录对象:"+JSON.toJSONString(myresorce));

//            String pathurl= ResourceUtils.getFile("calsspath:static/images").getAbsolutePath()+"/"+fileName;//通过ResourceUtils对象获取资源目录绝对路径
                try {
                    String pathurl= ResourceUtils.getFile("ruoyi-admin/src/main/resources/static/images").getAbsolutePath()+"/"+fileName;//通过ResourceUtils对象获取资源目录绝对路径
                    System.out.println("图片的全路径:"+pathurl);
                    //上传文件file到指定目录pathurl
                    file.transferTo(new File(pathurl));
                    //项目访问路径
                    fileurl=request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() +"/images/"+fileName;
                    System.out.println("图片的访问路径:"+fileurl);
                } catch (IOException e) {
                    e.printStackTrace();
                    System.err.println("写入文件夹时发生错误：" + e.getMessage());
                }
            }else {
                return AjaxResult.error("图片不能为空！");
            }
        }catch (Exception e){
            return AjaxResult.error("上传失败！");
        }
        return AjaxResult.success("上传成功",fileurl);
    }
}